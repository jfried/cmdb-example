# Simple CMDB example

This is a simple CMDB example that uses the default lookup path for the Rex CMDB.
To change the lookup path see the example on http://www.rexify.org/howtos/releases/0.46.html (CMDB path can now have variables).

## How does it work

If you active the at least the feature-set for 0.51 the CMDB is loaded by default. If you can do this, you can load the CMDB with the following code:

```perl
use Rex::CMDB;

set cmdb => {
   type => "YAML",
   path => "./cmdb",
};
```

In the example i added the *no_ssh* option in front of the task, so this task won't do an ssh connection to the server. With this it is easier to test and play with this example.

Some command to try:

```
rex setup
rex -E myenv setup
rex -E myenv -H server1 setup
```

## CMDB structure

The default CMDB is YAML based and the lookup is done in a given hierarchy. The default with the 0.51 feature flag is:

* cmdb/{operatingsystem}/{hostname}.yml
* cmdb/{operatingsystem}/default.yml
* cmdb/{environment}/{hostname}.yml
* cmdb/{environment}/default.yml
* cmdb/{hostname}.yml
* cmdb/default.yml

Where *{hostname}* is the hostname of the server given by ```hostname -s```.

If you don't use the 0.51 (or above) feature flag, the default is:

* cmdb/$environment/$server.yml
* cmdb/$environment/default.yml
* cmdb/$server.yml
* cmdb/default.yml

Where *$server* is the name (or ip) that is used by rex to establish the connection. (For example rex -H server1 $task).

So, if you want to set default values, you can set them in the *cmdb/default.yml* file. If you want to overwrite specific options per environment or per server you can create yaml files for them and overwrite these specific options.



